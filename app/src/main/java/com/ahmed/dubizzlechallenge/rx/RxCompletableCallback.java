package com.ahmed.dubizzlechallenge.rx;

import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableCompletableObserver;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class RxCompletableCallback extends DisposableCompletableObserver {

    private WeakReference<BaseNetworkingViewModel> viewModel;
    private int httpSelector, networkSelector, unexpectedErrorSelector;

    public RxCompletableCallback(BaseNetworkingViewModel viewModel, int httpSelector, int networkSelector, int unexpectedErrorSelector) {
        this.viewModel = new WeakReference<>(viewModel);
        this.httpSelector = httpSelector;
        this.networkSelector = networkSelector;
        this.unexpectedErrorSelector = unexpectedErrorSelector;
    }

    protected abstract void onSuccess();

    @Override
    public void onError(Throwable throwable) {
        new RxThrowableHelper().onError(throwable, viewModel.get(), httpSelector, unexpectedErrorSelector, networkSelector);
    }

    @Override
    public void onComplete() {
        onSuccess();
    }
}
