package com.ahmed.dubizzlechallenge.rx;

import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;
import com.ahmed.dubizzlechallenge.retrofit.ErrorModel;
import com.ahmed.dubizzlechallenge.retrofit.RetrofitException;

import java.io.IOException;

import timber.log.Timber;

import static java.net.Proxy.Type.HTTP;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class RxThrowableHelper {

    public void onError(Throwable throwable, BaseNetworkingViewModel viewModel, int httpSelector, int unexpectedErrorSelector, int networkSelector) {
        Timber.e("=================== onError: " + throwable.getLocalizedMessage());


        if (throwable instanceof RetrofitException) {
            RetrofitException error = (RetrofitException) throwable;
            switch (error.getKind()) {
                case HTTP: //non 2xx http error
                    try {
                        ErrorModel errorModel = error.getErrorBodyAs(ErrorModel.class);
                        viewModel.handlerSelector(errorModel.getErrors().get(0).getMessage(), httpSelector);
                    } catch (IOException e) {
                        viewModel.handlerSelector(error.getMessage(), unexpectedErrorSelector);
                    }
                    break;

                case NETWORK: //network and conversion error
                    viewModel.handlerSelector(error.getMessage(), networkSelector);
                    break;
                case UNEXPECTED:
                    viewModel.handlerSelector(error.getMessage(), unexpectedErrorSelector);
                    break;
            }
        } else {
            viewModel.handlerSelector(throwable.getLocalizedMessage(), unexpectedErrorSelector);
        }
    }

}
