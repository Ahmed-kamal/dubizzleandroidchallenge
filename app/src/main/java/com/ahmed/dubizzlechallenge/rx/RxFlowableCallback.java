package com.ahmed.dubizzlechallenge.rx;

import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class RxFlowableCallback<T> extends DisposableObserver<T> {
    private WeakReference<BaseNetworkingViewModel> viewModel;
    private int httpSelector, networkSelector, unexpectedErrorSelector;

    public RxFlowableCallback(BaseNetworkingViewModel viewModel, int httpSelector, int networkSelector, int unexpectedErrorSelector) {
        this.viewModel = new WeakReference<>(viewModel);
        this.httpSelector = httpSelector;
        this.networkSelector = networkSelector;
        this.unexpectedErrorSelector = unexpectedErrorSelector;
    }

    protected abstract void onSuccess(T t);

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    @Override
    public void onError(Throwable throwable) {
        Timber.e("=================== onError: " + throwable.getLocalizedMessage());
        new RxThrowableHelper().onError(throwable, viewModel.get(), httpSelector, unexpectedErrorSelector, networkSelector);
    }

    @Override
    public void onComplete() {
    }
}
