package com.ahmed.dubizzlechallenge.rx;

import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class RxSingleCallback<T> extends DisposableSingleObserver<T> {
    private WeakReference<BaseNetworkingViewModel> viewModel;
    private int httpSelector, networkSelector, unexpectedErrorSelector;

    public RxSingleCallback(BaseNetworkingViewModel viewModel, int httpSelector, int networkSelector, int unexpectedErrorSelector) {
        this.viewModel = new WeakReference<>(viewModel);
        this.httpSelector = httpSelector;
        this.networkSelector = networkSelector;
        this.unexpectedErrorSelector = unexpectedErrorSelector;
    }

    @Override
    public abstract void onSuccess(T t);

    @Override
    public void onError(Throwable throwable) {
        new RxThrowableHelper().onError(throwable, viewModel.get(), httpSelector, unexpectedErrorSelector, networkSelector);
    }
}
