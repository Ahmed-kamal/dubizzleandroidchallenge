package com.ahmed.dubizzlechallenge.injection.rx;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public enum SchedulerType {
    IO, COMPUTATION, UI
}
