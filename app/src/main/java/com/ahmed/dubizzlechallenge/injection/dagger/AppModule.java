package com.ahmed.dubizzlechallenge.injection.dagger;

import android.content.Context;

import com.ahmed.dubizzlechallenge.TMDBApplication;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Module
public class AppModule {

    @Provides
    Context provideContext (TMDBApplication application) {
        return application.getApplicationContext();
    }

    //Any more general App Implementation
}

