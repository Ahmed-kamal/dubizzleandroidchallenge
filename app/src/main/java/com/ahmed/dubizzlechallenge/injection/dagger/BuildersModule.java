package com.ahmed.dubizzlechallenge.injection.dagger;

import com.ahmed.dubizzlechallenge.injection.api.ApiModule;
import com.ahmed.dubizzlechallenge.injection.moviedetails.MovieDetailsModule;
import com.ahmed.dubizzlechallenge.injection.topmovies.TopMoviesModule;
import com.ahmed.dubizzlechallenge.injection.network.NetworkModule;
import com.ahmed.dubizzlechallenge.presentation.moviedetails.MovieDetailsActivity;
import com.ahmed.dubizzlechallenge.presentation.top.TopMoviesActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Module
public abstract class BuildersModule {

    //Inject Views using contribute android injector
    @ContributesAndroidInjector(modules = {TopMoviesModule.class , ApiModule.class , NetworkModule.class})
    abstract TopMoviesActivity topMoviesActivity();

    @ContributesAndroidInjector(modules = {MovieDetailsModule.class , ApiModule.class , NetworkModule.class})
    abstract MovieDetailsActivity movieDetailsActivity();
}
