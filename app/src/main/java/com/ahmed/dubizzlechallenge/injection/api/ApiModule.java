package com.ahmed.dubizzlechallenge.injection.api;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;
import com.ahmed.dubizzlechallenge.injection.network.NetworkModule;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */
@Module(includes = NetworkModule.class)
public class ApiModule {
    //Provide all Retrofit api injections
    @Provides
    MoviesService moviesService(Retrofit retrofit) {
        return retrofit.create(MoviesService.class);
    }
}
