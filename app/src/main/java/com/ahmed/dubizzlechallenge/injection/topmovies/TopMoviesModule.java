package com.ahmed.dubizzlechallenge.injection.topmovies;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;
import com.ahmed.dubizzlechallenge.presentation.top.TopMoviesViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */
@Module
public class TopMoviesModule {
    @Provides
    TopMoviesViewModelFactory provideLatestMoviesViewModelFactory (MoviesService service) {
        return new TopMoviesViewModelFactory(service);
    }
}
