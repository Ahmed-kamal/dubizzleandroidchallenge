package com.ahmed.dubizzlechallenge.injection.rx;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.ahmed.dubizzlechallenge.injection.rx.SchedulerType.COMPUTATION;
import static com.ahmed.dubizzlechallenge.injection.rx.SchedulerType.IO;
import static com.ahmed.dubizzlechallenge.injection.rx.SchedulerType.UI;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Module
public class SchedulersModule {
    @Provides
    @RunOn(IO)
    Scheduler provideIo(){
        return Schedulers.io();
    }

    @Provides
    @RunOn(COMPUTATION)
    Scheduler provideComputation() {
        return Schedulers.computation();
    }

    @Provides
    @RunOn(UI)
    Scheduler provideUi() {
        return AndroidSchedulers.mainThread();
    }
}
