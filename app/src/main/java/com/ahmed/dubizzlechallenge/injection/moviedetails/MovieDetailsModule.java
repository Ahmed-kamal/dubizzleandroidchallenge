package com.ahmed.dubizzlechallenge.injection.moviedetails;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;
import com.ahmed.dubizzlechallenge.presentation.moviedetails.MovieDetailsViewModelFactory;

import dagger.Module;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */
@Module
public class MovieDetailsModule {
    MovieDetailsViewModelFactory provideMovieDetailsViewModelFactory(MoviesService service) {
        return new MovieDetailsViewModelFactory(service);
    }
}
