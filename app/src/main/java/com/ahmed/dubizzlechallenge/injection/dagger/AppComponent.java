package com.ahmed.dubizzlechallenge.injection.dagger;

import com.ahmed.dubizzlechallenge.TMDBApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class , AppModule.class , BuildersModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(TMDBApplication application);
        AppComponent build();
    }

    void inject(TMDBApplication application);
}
