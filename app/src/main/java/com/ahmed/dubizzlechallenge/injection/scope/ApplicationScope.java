package com.ahmed.dubizzlechallenge.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ApplicationScope {}
