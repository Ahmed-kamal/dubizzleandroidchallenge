package com.ahmed.dubizzlechallenge.injection.network;

import com.ahmed.dubizzlechallenge.BuildConfig;
import com.ahmed.dubizzlechallenge.injection.scope.ApiScope;
import com.ahmed.dubizzlechallenge.rx.RxErrorHandlingCallAdapterFactory;
import com.ahmed.dubizzlechallenge.utils.PreferanceUtils;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import timber.log.Timber;

import static com.ahmed.dubizzlechallenge.utils.Const.BASE_URL;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

@Module
public class NetworkModule {


    @Provides
    public HttpLoggingInterceptor loggingInterceptor() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor((logMessage) -> Timber.e(logMessage));
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }


    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor) {

        return new OkHttpClient.Builder().addInterceptor(loggingInterceptor).
                connectTimeout(30, TimeUnit.SECONDS).build();
    }



    @Provides
    public Retrofit retrofit(OkHttpClient client) {

        return new Retrofit.Builder().baseUrl(BASE_URL).
                addConverterFactory(ScalarsConverterFactory.create()).
                addConverterFactory(GsonConverterFactory.create())
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.
                        createWithScheduler(Schedulers.io())).client(client).build();
    }
}
