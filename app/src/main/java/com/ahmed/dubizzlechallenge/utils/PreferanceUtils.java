package com.ahmed.dubizzlechallenge.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class PreferanceUtils {
    private Context context;
    private SharedPreferences sharedPreference;


    public PreferanceUtils(Context context) {
        this.context = context;
        this.sharedPreference = context.getSharedPreferences(DefaultKeys.DEFAULT_SHARED_PREFERENCE, Context.MODE_PRIVATE);
    }

    public PreferanceUtils(Context context, String sharedPreferenceName) {
        this.context = context;
        this.sharedPreference = context.getSharedPreferences(sharedPreferenceName, Context.MODE_PRIVATE);
    }

    public boolean editValue(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public boolean editValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public boolean editValue(String key, float value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public boolean editValue(String key, int value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public boolean editValue(String key, long value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreference.getBoolean(key, false);
    }

    public String getString(String key) {
        return sharedPreference.getString(key, null);
    }

    public float getFloat(String key) {
        return sharedPreference.getFloat(key, 0.0f);
    }

    public int getInt(String key) {
        return sharedPreference.getInt(key, 0);
    }

    public long getLong(String key) {
        return sharedPreference.getLong(key, 0);
    }

    public Set<String> getStringSet(String key) {
        return sharedPreference.getStringSet(key, null);
    }


    public interface DefaultKeys {
        String DEFAULT_SHARED_PREFERENCE = "DefaultPreference";
        String PREF_TOKEN = "user_token";
        String PREFS_USER_NAME = "user_name";
        String PREFS_USER_EMAIL = "user_email";
    }
}
