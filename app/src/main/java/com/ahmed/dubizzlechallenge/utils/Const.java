package com.ahmed.dubizzlechallenge.utils;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class Const {
    public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
    public static final String TMDB_API_KEY = "38a23b99537fb38df50ecff913b816ef";
    public static final String DATE_FROMAT = "YYYY-MM-dd";
    public static final String TMDB_IMG_URL = "http://image.tmdb.org/t/p/w500";
}
