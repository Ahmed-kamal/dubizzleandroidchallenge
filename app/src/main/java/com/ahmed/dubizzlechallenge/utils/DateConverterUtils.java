package com.ahmed.dubizzlechallenge.utils;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import timber.log.Timber;

import static com.ahmed.dubizzlechallenge.utils.Const.DATE_FROMAT;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */

public class DateConverterUtils {

    public static int getYearFromDate(String fullDate) {
        int year = 0;

        try {
            if (fullDate != null) {
                @SuppressLint("SimpleDateFormat")
                DateFormat dateFormat = new SimpleDateFormat(DATE_FROMAT);
                Date date = dateFormat.parse(fullDate);
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                return calendar.get(Calendar.YEAR);
            } else {
                return year;
            }
        } catch (ParseException e) {
            Timber.e(e.getMessage());
        }
        return year;
    }
}
