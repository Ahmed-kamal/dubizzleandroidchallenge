package com.ahmed.dubizzlechallenge.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class ErrorModel {
    private boolean error;
    private List<Error> errors;

    public ErrorModel(boolean error, List<Error> errors) {
        this.error = error;
        this.errors = errors;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public String getErrorsFormatted() {
        if (isError() && errors != null) {
            String errorMessages = "Errors:";
            for (Error e : errors) {
                errorMessages.concat(String.format("    ErrorCode: %s, Error Message: %s", e.getCode(), e.getMessage()));
            }
            return errorMessages;
        } else {
            return "no errors";
        }
    }

    public class Error {
        @SerializedName("code")
        private String code;
        @SerializedName("message")
        private String message;

        public Error(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
