package com.ahmed.dubizzlechallenge.presentation.top;

import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;

import javax.inject.Inject;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class TopMoviesViewModelFactory implements ViewModelProvider.Factory {

    private MoviesService service;

    @Inject
    public TopMoviesViewModelFactory(MoviesService service) {
        this.service = service;
    }

    @NonNull
    @Override
    public TopMoviesViewModel create(@NonNull Class modelClass) {
        return new TopMoviesViewModel(service);
    }
}
