package com.ahmed.dubizzlechallenge.presentation.top;

import android.arch.lifecycle.MutableLiveData;

import com.ahmed.dubizzlechallenge.R;
import com.ahmed.dubizzlechallenge.data.api.MoviesService;
import com.ahmed.dubizzlechallenge.data.model.topmovies.MoviesResult;
import com.ahmed.dubizzlechallenge.data.model.topmovies.TopRatedMoviesModel;
import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;
import com.ahmed.dubizzlechallenge.rx.RxJavaUtils;
import com.ahmed.dubizzlechallenge.rx.RxSingleCallback;
import com.ahmed.dubizzlechallenge.utils.DateConverterUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel.ErrorHandlers.SNACK_BAR;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

class TopMoviesViewModel extends BaseNetworkingViewModel {

    private MoviesService moviesService;
    private MutableLiveData<List<MoviesResult>> topMoviesMutable = new MutableLiveData<>();
    private MutableLiveData<Integer> anyErrorMsgMutable = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loadingStatus = new MutableLiveData<>();

    MutableLiveData<List<MoviesResult>> getTopMoviesMutable() {
        return topMoviesMutable;
    }


    MutableLiveData<Integer> getAnyErrorMsgMutable() {
        return anyErrorMsgMutable;
    }

    MutableLiveData<Boolean> getLoadingStatus() {
        return loadingStatus;
    }

    TopMoviesViewModel(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    void getTopMovies() {
        getCompositeDisposable().add(moviesService.getTopMovies().
                compose(RxJavaUtils.applySingleSchedulers()).
                doOnSubscribe(loading -> loadingStatus.setValue(true)).
                doAfterTerminate(() -> loadingStatus.setValue(false)).
                subscribeWith(new RxSingleCallback<TopRatedMoviesModel>(this, SNACK_BAR, SNACK_BAR, SNACK_BAR) {
            @Override
            public void onSuccess(TopRatedMoviesModel latestMovieModels) {
                topMoviesMutable.setValue(latestMovieModels.getResults());
            }
        }));
    }

    void filterByYear(String minYear, String maxYear) {
        if(!minYear.isEmpty() && !maxYear.isEmpty()) {
            int min = Integer.parseInt(minYear);
            int max = Integer.parseInt(maxYear);

            if(min != max && min < max) {
                getCompositeDisposable().add(moviesService.getTopMovies().
                        toFlowable().
                        flatMapIterable(TopRatedMoviesModel::getResults).
                        filter(topRatedMoviesModel -> DateConverterUtils.getYearFromDate(topRatedMoviesModel.getReleaseDate()) >
                                min && DateConverterUtils.getYearFromDate(topRatedMoviesModel.getReleaseDate()) < max).
                        toList().
                        doOnSubscribe(loading -> loadingStatus.postValue(true)).
                        doAfterTerminate(() -> loadingStatus.postValue(false)).
                        compose(RxJavaUtils.applySingleSchedulers()).
                        subscribeWith(new RxSingleCallback<List<MoviesResult>>(this, SNACK_BAR, SNACK_BAR , SNACK_BAR) {
                            @Override
                            public void onSuccess(List<MoviesResult> moviesResults) {
                                topMoviesMutable.setValue(moviesResults);
                            }
                        }));
            } else {
                anyErrorMsgMutable.setValue(R.string.year_error);
            }
        } else {
            anyErrorMsgMutable.setValue(R.string.mandatory);
        }
    }
}
