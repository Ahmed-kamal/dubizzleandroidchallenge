package com.ahmed.dubizzlechallenge.presentation.bases;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class BaseBindingActivity<B extends ViewDataBinding> extends AppCompatActivity {

    private B binding;

    @LayoutRes
    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , getLayoutResId());
    }

    public B getBinding() {return binding;}
}
