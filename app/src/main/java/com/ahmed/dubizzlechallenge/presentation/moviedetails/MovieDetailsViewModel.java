package com.ahmed.dubizzlechallenge.presentation.moviedetails;

import android.arch.lifecycle.MutableLiveData;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;
import com.ahmed.dubizzlechallenge.data.model.moviedetails.MovieDetailsModel;
import com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel;
import com.ahmed.dubizzlechallenge.rx.RxJavaUtils;
import com.ahmed.dubizzlechallenge.rx.RxSingleCallback;

import static com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel.ErrorHandlers.SNACK_BAR;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */

public class MovieDetailsViewModel extends BaseNetworkingViewModel {

    private MoviesService service;
    private MutableLiveData<MovieDetailsModel> movieDetailsMutable = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loadingStatus = new MutableLiveData<>();

    public MutableLiveData<MovieDetailsModel> getMovieDetailsMutable() {
        return movieDetailsMutable;
    }

    public MutableLiveData<Boolean> getLoadingStatus() {
        return loadingStatus;
    }

    MovieDetailsViewModel(MoviesService service) {
        this.service = service;
    }

    void getMovieDetails(int id) {
        getCompositeDisposable().add(service.getMovieDetails(id).
                        compose(RxJavaUtils.applySingleSchedulers()).
                doOnSubscribe(loading -> loadingStatus.setValue(true)).
                doAfterTerminate(() -> loadingStatus.setValue(false)).
                subscribeWith(new RxSingleCallback<MovieDetailsModel>(this, SNACK_BAR, SNACK_BAR, SNACK_BAR) {
            @Override
            public void onSuccess(MovieDetailsModel movieDetailsModel) {
                movieDetailsMutable.setValue(movieDetailsModel);
            }
        }));
    }
}
