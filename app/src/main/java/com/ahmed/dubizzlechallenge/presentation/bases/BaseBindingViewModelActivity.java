package com.ahmed.dubizzlechallenge.presentation.bases;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.ahmed.dubizzlechallenge.R;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class BaseBindingViewModelActivity<VM extends BaseViewModel , B extends ViewDataBinding> extends BaseBindingActivity<B> {

    private VM viewModel;

    public abstract VM setupViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = setupViewModel();
        viewModel.getSnackBarLiveEvent().observe(this, this::showSnackBar);
        viewModel.getToastLiveEvent().observe(this, this::showToast);
    }

    protected void showSnackBar(String message) {
        Snackbar.make(getBinding().getRoot(), message, Snackbar.LENGTH_LONG).show();
    }

    protected void showSnackBarResource(int message) {
        Snackbar.make(getBinding().getRoot(), message, Snackbar.LENGTH_LONG).show();
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public VM getViewModel() {
        return viewModel;
    }
}
