package com.ahmed.dubizzlechallenge.presentation.top;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ahmed.dubizzlechallenge.R;
import com.ahmed.dubizzlechallenge.data.model.topmovies.MoviesResult;
import com.ahmed.dubizzlechallenge.databinding.TopMoviesItemBinding;
import com.ahmed.dubizzlechallenge.presentation.bases.BaseRecyclerViewAdapter;
import com.bumptech.glide.Glide;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class TopMoviesAdapter extends BaseRecyclerViewAdapter<TopMoviesAdapter.TopMoviesViewHolder> {

    private List<MoviesResult> topRatedMoviesModelList;
    private Context ctx;

    public TopMoviesAdapter(List<MoviesResult> topRatedMoviesModelList, WeakReference<Context> ctx) {
        this.topRatedMoviesModelList = topRatedMoviesModelList;
        this.ctx = ctx.get();
    }

    @Override
    public TopMoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TopMoviesItemBinding binding = DataBindingUtil.inflate(inflater , R.layout.top_movies_item, parent , false);
        return new TopMoviesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        TopMoviesViewHolder topMoviesViewHolder = (TopMoviesViewHolder) holder; //safe cast
        topMoviesViewHolder.bind(position);
    }

    void replaceData(List<MoviesResult> topRatedMoviesModelList) {
        this.topRatedMoviesModelList.clear();
        this.topRatedMoviesModelList.addAll(topRatedMoviesModelList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return topRatedMoviesModelList.size();
    }

    class TopMoviesViewHolder extends RecyclerView.ViewHolder {

        TopMoviesItemBinding binding;

        public TopMoviesViewHolder(TopMoviesItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(int position) {
            binding.setMovieName(topRatedMoviesModelList.get(position).getTitle());
            Glide.with(ctx).load(topRatedMoviesModelList.get(position).getPosterPath()).into(binding.popularMoviesImageView);
        }
    }
}
