package com.ahmed.dubizzlechallenge.presentation.bases;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class BaseBindingViewModelFragment<VM extends BaseViewModel , B extends ViewDataBinding> extends BaseBindingFragment<B> {

    private VM viewModel;
    public abstract VM setupViewModel();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = setupViewModel();

        viewModel.getSnackBarLiveEvent().observe(this, this::showSnackBar);
        viewModel.getSnackBarLiveEventResource().observe(this, this::showSnackBarFromResouce);
        viewModel.getToastLiveEvent().observe(this, this::showToast);
    }

    public void showSnackBar(String message) {
        Snackbar.make(getBinding().getRoot(), message, Snackbar.LENGTH_LONG).show();
    }

    public void showSnackBarFromResouce(int message) {
        Snackbar.make(getBinding().getRoot(), getResources().getString(message), Snackbar.LENGTH_LONG).show();
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
