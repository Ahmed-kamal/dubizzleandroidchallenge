package com.ahmed.dubizzlechallenge.presentation.moviedetails;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ahmed.dubizzlechallenge.data.api.MoviesService;

import javax.inject.Inject;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */

public class MovieDetailsViewModelFactory implements ViewModelProvider.Factory {

    private MoviesService service;

    @Inject
    public MovieDetailsViewModelFactory(MoviesService service) {
        this.service = service;
    }

    @NonNull
    @Override
    public MovieDetailsViewModel create(@NonNull Class modelClass) {
        return new MovieDetailsViewModel(service);
    }
}
