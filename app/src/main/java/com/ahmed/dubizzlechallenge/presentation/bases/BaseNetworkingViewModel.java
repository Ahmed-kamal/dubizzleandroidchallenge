package com.ahmed.dubizzlechallenge.presentation.bases;

import static com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel.ErrorHandlers.SNACK_BAR;
import static com.ahmed.dubizzlechallenge.presentation.bases.BaseNetworkingViewModel.ErrorHandlers.TOAST;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class BaseNetworkingViewModel extends BaseViewModel {

    public void handlerSelector(String message, int handler) {
        switch (handler) {
            case SNACK_BAR:
                getSnackBarLiveEvent().setValue(message);
                break;
            case TOAST:
                getToastLiveEvent().setValue(message);
                break;
            default:
                getSnackBarLiveEvent().setValue(message);
                break;
        }
    }

    protected boolean hasContent() {
        return false;
    }
    public interface ErrorHandlers {
        int SNACK_BAR = 0;
        int TOAST = 1;
    }
}
