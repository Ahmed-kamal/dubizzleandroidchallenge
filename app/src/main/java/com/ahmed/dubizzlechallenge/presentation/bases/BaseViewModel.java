package com.ahmed.dubizzlechallenge.presentation.bases;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.CallSuper;

import com.ahmed.dubizzlechallenge.rx.SingleLiveEvent;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class BaseViewModel extends ViewModel {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SingleLiveEvent<String> snackBarLiveEvent; //used to send snack bar messages
    private SingleLiveEvent<Integer> snackBarLiveEventResource; //used to send snack bar messages
    private SingleLiveEvent<String> toastLiveEvent; //used to send toast messages
    private SingleLiveEvent<Integer> toastLiveEventResource; //used to send toast messages

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @CallSuper
    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }

    public SingleLiveEvent<String> getSnackBarLiveEvent() {
        if (snackBarLiveEvent == null) {
            snackBarLiveEvent = new SingleLiveEvent<>();
        }

        return snackBarLiveEvent;
    }

    public SingleLiveEvent<Integer> getSnackBarLiveEventResource() {
        if (snackBarLiveEventResource == null) {
            snackBarLiveEventResource = new SingleLiveEvent<>();
        }

        return snackBarLiveEventResource;
    }

    public SingleLiveEvent<String> getToastLiveEvent() {
        if (toastLiveEvent == null) {
            toastLiveEvent = new SingleLiveEvent<>();
        }

        return toastLiveEvent;
    }

    public SingleLiveEvent<Integer> getToastLiveEventResource() {
        if (toastLiveEventResource == null) {
            toastLiveEventResource = new SingleLiveEvent<>();
        }

        return toastLiveEventResource;
    }
}
