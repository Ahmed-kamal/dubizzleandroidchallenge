package com.ahmed.dubizzlechallenge.presentation.bases;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public abstract class BaseBindingFragment<B extends ViewDataBinding> extends Fragment {

    private B binding;

    @LayoutRes
    protected abstract int getFragmentLayoutResource();

    @CallSuper
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater , getFragmentLayoutResource() , container , false);
        return binding.getRoot();
    }

    public B getBinding() {
        return binding;
    }
}
