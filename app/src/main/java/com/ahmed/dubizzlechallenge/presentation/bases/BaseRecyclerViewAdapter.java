package com.ahmed.dubizzlechallenge.presentation.bases;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */

public abstract class BaseRecyclerViewAdapter<VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private RecyclerViewListener.OnItemClickListener itemClickListener;
    private RecyclerViewListener.OnItemLongClickListener itemLongClickListener;

    public void setOnItemClickListener
            (@NonNull RecyclerViewListener.OnItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public void setOnItemLongClickListener
            (@NonNull RecyclerViewListener.OnItemLongClickListener itemLongClickListener)
    {
        this.itemLongClickListener = itemLongClickListener;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        if (itemClickListener != null) {
            holder.itemView.setOnClickListener(view -> itemClickListener.OnItemClick(view, position));
        }
        if (itemLongClickListener != null) {
            holder.itemView.setOnLongClickListener(view -> {
                itemLongClickListener.OnItemLongClick(view, position);
                return true;
            });
        }
    }
}
