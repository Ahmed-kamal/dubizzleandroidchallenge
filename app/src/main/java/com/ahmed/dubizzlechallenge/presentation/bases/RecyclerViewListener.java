package com.ahmed.dubizzlechallenge.presentation.bases;

import android.view.View;

/**
 * Created by Ahmed Kamal on 12/05/2018.
 */

public interface RecyclerViewListener {
    @FunctionalInterface
    public interface OnItemClickListener {
        void OnItemClick(View view, int position);
    }

    @FunctionalInterface
    interface OnItemLongClickListener {
        void OnItemLongClick(View view, int position);
    }
}
