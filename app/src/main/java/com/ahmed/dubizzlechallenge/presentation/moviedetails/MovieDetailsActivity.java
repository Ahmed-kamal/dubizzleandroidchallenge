package com.ahmed.dubizzlechallenge.presentation.moviedetails;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ahmed.dubizzlechallenge.R;
import com.ahmed.dubizzlechallenge.data.model.moviedetails.MovieDetailsModel;
import com.ahmed.dubizzlechallenge.databinding.ActivityMovieDetailsBinding;
import com.ahmed.dubizzlechallenge.presentation.bases.BaseBindingViewModelActivity;
import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.ahmed.dubizzlechallenge.presentation.top.TopMoviesActivity.MOVIE_ID;

public class MovieDetailsActivity extends BaseBindingViewModelActivity<MovieDetailsViewModel , ActivityMovieDetailsBinding> {

    @Inject
    MovieDetailsViewModelFactory factory;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_movie_details;
    }

    @Override
    public MovieDetailsViewModel setupViewModel() {
        return ViewModelProviders.of(this , factory).get(MovieDetailsViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        getViewModel().getLoadingStatus().observe(this , isLoading -> getBinding().
                movieDetailsProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE));

        getViewModel().getMovieDetailsMutable().observe(this , movieDetailsModel -> {
            MovieDetailsActivity.this.getBinding().setMoveDetailsModel(movieDetailsModel);
            Glide.with(this).load(movieDetailsModel.getPosterPath()).into(getBinding().imageView);
        });

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Movie Details");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(getIntent() != null) getViewModel().getMovieDetails(Integer.parseInt(getIntent().getStringExtra(MOVIE_ID)));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
