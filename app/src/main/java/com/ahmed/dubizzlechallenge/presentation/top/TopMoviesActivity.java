package com.ahmed.dubizzlechallenge.presentation.top;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ahmed.dubizzlechallenge.R;
import com.ahmed.dubizzlechallenge.data.model.topmovies.MoviesResult;
import com.ahmed.dubizzlechallenge.databinding.ActivityTopMoviesBinding;
import com.ahmed.dubizzlechallenge.databinding.CustomDialogBinding;
import com.ahmed.dubizzlechallenge.presentation.bases.BaseBindingViewModelActivity;
import com.ahmed.dubizzlechallenge.presentation.moviedetails.MovieDetailsActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class TopMoviesActivity extends BaseBindingViewModelActivity<TopMoviesViewModel, ActivityTopMoviesBinding> {

    @Inject
    TopMoviesViewModelFactory factory;

    private RecyclerView recyclerView;
    private TopMoviesAdapter adapter;
    public static final String MOVIE_ID = "movie_id";

    @Override
    public TopMoviesViewModel setupViewModel() {
        return ViewModelProviders.of(this , factory).get(TopMoviesViewModel.class);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_top_movies;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        getBinding().setView(this);
        initRv();

        getViewModel().getTopMoviesMutable().observe(this , this::showData);
        getViewModel().getAnyErrorMsgMutable().observe(this , this::showSnackBarResource);

        getViewModel().getLoadingStatus().observe(this , isLoading -> getBinding().
                progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE));

        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle("Top Movies");
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        getViewModel().getTopMovies();
    }

    private void initRv() {
        recyclerView = getBinding().latestMoviesRv;
        recyclerView.setLayoutManager(new GridLayoutManager(this , 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        adapter = new TopMoviesAdapter(new ArrayList<>() , new WeakReference<>(this));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((view, position) -> {
            //Navigate to the second activity
            Intent intent = new Intent(TopMoviesActivity.this , MovieDetailsActivity.class);
            intent.putExtra(MOVIE_ID , String.valueOf(getViewModel().getTopMoviesMutable().getValue().get(position).getId()));
            startActivity(intent);
        });
    }

    private void showData(List<MoviesResult> moviesResults) {
        adapter.replaceData(moviesResults);
        if(moviesResults.size() == 0) getBinding().noResults.setText(R.string.no_result);
        else getBinding().noResults.setText("");
    }

    public void filterClicked() {
        CustomDialogBinding binding = CustomDialogBinding.
                inflate(LayoutInflater.from(this));

        binding.setView(this);

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .customView(binding.getRoot(), false)
                .show();

        binding.dialogCancel.setOnClickListener(view -> dialog.dismiss());
        binding.dialogSearch.setOnClickListener(view -> {
            TopMoviesActivity.this.getViewModel().filterByYear(binding.minYearEt.getText().toString(),
                    binding.maxYearEt.getText().toString());
            dialog.dismiss();
        });
    }
}
