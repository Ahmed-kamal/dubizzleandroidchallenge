package com.ahmed.dubizzlechallenge;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;

import com.ahmed.dubizzlechallenge.injection.dagger.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;
import timber.log.Timber;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public class TMDBApplication extends Application implements HasActivityInjector , HasFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingFragmentAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        DaggerAppComponent.builder().application(this).build().inject(this);

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityAndroidInjector;
    }

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return dispatchingFragmentAndroidInjector;
    }
}
