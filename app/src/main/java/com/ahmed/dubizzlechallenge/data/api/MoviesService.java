package com.ahmed.dubizzlechallenge.data.api;

import com.ahmed.dubizzlechallenge.data.model.topmovies.TopRatedMoviesModel;
import com.ahmed.dubizzlechallenge.data.model.moviedetails.MovieDetailsModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.ahmed.dubizzlechallenge.utils.Const.TMDB_API_KEY;

/**
 * Created by Ahmed Kamal on 11/05/2018.
 */

public interface MoviesService {
    @GET("top_rated?api_key="+TMDB_API_KEY)
    Single<TopRatedMoviesModel> getTopMovies();

    @GET("{movie_id}?api_key="+ TMDB_API_KEY)
    Single<MovieDetailsModel> getMovieDetails(@Path("movie_id") int movieId);
}
